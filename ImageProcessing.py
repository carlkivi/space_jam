import cv2
import json
from functools import partial
import numpy as np
import pyrealsense2 as rs
import time
import imutils
import math


#global variables for use in main
ball_center = None
pillar_center = None


frames = None

def save_colors(port):

    # Load saved color values froms colors.json
    try:
        with open("colors.json", "r") as f:
            saved_colors = json.loads(f.read())
    except FileNotFoundError:
        saved_colors = {}

    print("Saved color values: ", saved_colors)
    color = input("What color to threshold: ")

    # Read color values from colors.json or initialize new values
    if color in saved_colors:
        filters = saved_colors[color]
    else:
        filters = {
            "min": [0, 0, 0],  # HSV minimum values
            "max": [179, 255, 255]  # HSV maximum values
        }

    def save():
        saved_colors[color] = filters

        with open("colors.json", "w") as f:
            f.write(json.dumps(saved_colors))

    def update_range(edge, channel, value):
        # edge = "min" or "max"
        # channel = 0, 1, 2 (H, S, V)
        # value = new slider value
        filters[edge][channel] = value


    # Create sliders to filter colors from image
    cv2.namedWindow("mask", cv2.WINDOW_AUTOSIZE)
    cv2.namedWindow("trackbars")



    # createTrackbar(name, window name, initial value, max value, function to call on change)
    cv2.createTrackbar("h_min", "trackbars", filters["min"][0], 179, partial(update_range, "min", 0))
    cv2.createTrackbar("s_min", "trackbars", filters["min"][1], 255, partial(update_range, "min", 1))
    cv2.createTrackbar("v_min", "trackbars", filters["min"][2], 255, partial(update_range, "min", 2))
    cv2.createTrackbar("h_max", "trackbars", filters["max"][0], 179, partial(update_range, "max", 0))
    cv2.createTrackbar("s_max", "trackbars", filters["max"][1], 255, partial(update_range, "max", 1))
    cv2.createTrackbar("v_max", "trackbars", filters["max"][2], 255, partial(update_range, "max", 2))

    # Start video capture
    #cap = cv2.VideoCapture(port)
    #set higher resolution for better image quality
    """cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)"""
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.color, 960, 540, rs.format.bgr8, 60)

    # Start streaming
    pipeline.start(config)

    print("If you want to save specific values press \"s\"")
    print("If you want exit color saving mode press \"q\"")
    while True:
        # 1. OpenCV gives you a BGR image
       # _, bgr = cap.read()

        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
        bgr = np.asanyarray(color_frame.get_data())
        #cv2.namedWindow("bgr", cv2.WINDOW_AUTOSIZE)
        #cv2.imshow("bgr", bgr)

        # 2. Convert BGR to HSV where color distributions are better
        hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)
        #cv2.imshow("hsv", hsv)

        # 3. Use filters on HSV image
        mask = cv2.inRange(hsv, tuple(filters["min"]), tuple(filters["max"]))
        #mask = cv2.erode(mask, None, iterations=2)
        #mask = cv2.dilate(mask, None, iterations=2)
        cv2.imshow("mask", mask)

        key = cv2.waitKey(10)

        if key & 0xFF == ord("s"):
            save()

        if key & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            break

    #cap.release()
    pipeline.stop()

def read_thresholds():
    saved_colors = {}
    try:
        with open("colors.json", "r") as f:
            saved_colors = json.loads(f.read())
    except FileNotFoundError:
        saved_colors = {}
    return saved_colors

#for this function frame should already be in hsv colorspace
#will return the largest contour found on the image
def find_object(target, frame):
    #get the color values and create a mask
    target_color = read_thresholds()[target]

    mask = cv2.inRange(frame, tuple(target_color["min"]), tuple(target_color["max"]))
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    #find all the contours on the mask
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)


    if len(contours) > 0:

        #find biggest contour and find its centerpoint
        c = max(contours, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))

        return [center, (x, y), radius]

    else:
        return

#tused this for testing realsense camera
def camera_sense():
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming
    pipeline.start(config)

    try:
        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            #nii saab kauguse
            kaugus = depth_frame.get_distance(10,10)

            color_frame = frames.get_color_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            # Stack both images horizontallycc
            #images = np.hstack((color_image, depth_colormap))

            # Show images
            cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.namedWindow("RealDepth", cv2.WINDOW_AUTOSIZE)
            hsv = cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV)
            cv2.imshow("hsv", hsv)
            cv2.imshow('RealSense', color_image)
            cv2.imshow("RealDepth", depth_colormap)


            key = cv2.waitKey(10)

            if key & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break

    finally:

        # Stop streaming
        pipeline.stop()


#in the final version this function should be consantly running in a separate thread
def robo_vision():
    global ball_center, pillar_center
    # set up the camera and start streaming data
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 1280, 720, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 30)
    pipeline.start(config)
    cv2.namedWindow("bgr", cv2.WINDOW_AUTOSIZE)

    while True:
        #get frames and convert them to hsv colorspace
        frames = pipeline.wait_for_frames()
        bgr = frames.get_color_frame()
        bgr = np.asanyarray(bgr.get_data())
        hsv = cv2.cvtColor(bgr, cv2.COLOR_BGR2HSV)

        try:
            #find stuff
            balls = find_object("green", hsv)
            ball_center = balls[0]
            #pillar_bloo = find_object("blue", hsv)
            #pillar_center = pillar_bloo[0]

            #this draws reen lines around green balls on the bgr image
            if balls[2] > 10:
                cv2.circle(bgr, (round(balls[1][0]), round(balls[1][1])), round(balls[2]), (0, 255, 255), 2)
                cv2.circle(bgr, ball_center, 5, (0, 0, 255), -1)
            #cv2.drawContours(bgr, balls, 0, (0,255,0), 3)
            #this draws a line around a blue pillar
            #cv2.drawContours(bgr, pillar_bloo, 0, (255,0,0,), 3)



        except:
            pass

        #getting coordinates of contours and calculating distance goes here



        #shows the image
        cv2.imshow("bgr", bgr)

        """key = cv2.waitKey(10)
        if key & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            break"""

        time.sleep(0.01)
    #this closes the the datastream from the camera
    pipeline.stop()

def get_screen_center():
    return rs.video_frame.get_width(frames)

def rs_view():
    # Configure depth and color streams
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming
    pipeline.start(config)

    try:
        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            depth_frame = frames.get_depth_frame()
            color_frame = frames.get_color_frame()
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())

            # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
            depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

            # Stack both images horizontally
            images = np.hstack((color_image, depth_colormap))
            print(color_image)
            break
            # Show images
            #cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            #cv2.imshow('RealSense', color_image)
            #cv2.waitKey(1)

    finally:

        # Stop streaming
        pipeline.stop()

rs_view()