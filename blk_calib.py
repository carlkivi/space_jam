import cv2
import pyrealsense2 as rs
import numpy as np
import os
import json

path = os.path.dirname(os.path.realpath(__file__))
file = path + "/colors.json"
try:
    with open(file, "r") as f:
        saved_colors = json.loads(f.read())
except FileNotFoundError:
    saved_colors = {}

def find_object(target, hsv):
    # get color and create mask
    target_color = saved_colors[target]
    mask = cv2.inRange(hsv, tuple(target_color["min"]), tuple(target_color["max"]))
    """mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)"""
    # find all the contours on the mask
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        # find biggest contour and find its centerpoint
        c = max(contours, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))
        return [center, (x, y), radius]
    else:
        return None

def find_white(mask):
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        # find biggest contour and find its centerpoint
        c = max(contours, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))
        return [center, (x, y), radius, c]
    else:
        return None

def black_object(frame, n):
    contours, hierarchy = cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) > 0:
        # find biggest contour and find its centerpoint
        c = sorted(contours, key=cv2.contourArea, reverse=True)

        maxc = max(c[n].tolist())[0]
        minc = min(c[n].tolist())[0]
        #print(min(c[n].tolist()))

        ((x, y), radius) = cv2.minEnclosingCircle(c[n])
        M = cv2.moments(c[n])
        center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))
        return [center, (x, y), radius, maxc, minc, c[n]]
    else:
        return None
x = 30

def update(new):
    global x
    x = new
    return

screen_res = (960, 540)
depth_res = (1280, 720)

pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, screen_res[0], screen_res[1], rs.format.bgr8, 60)
config.enable_stream(rs.stream.depth, depth_res[0], depth_res[1], rs.format.z16, 30)
cv2.namedWindow("trackbar")
cv2.namedWindow("blk")
cv2.namedWindow("n")
cv2.namedWindow("wht")
cv2.namedWindow("combined")

cv2.createTrackbar("thresh", "trackbar", x, 254, update)

profile = pipeline.start(config)
sensor = profile.get_device().query_sensors()[1]
sensor.set_option(rs.option.enable_auto_exposure, False)
sensor.set_option(rs.option.enable_auto_white_balance, False)
while True:
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    color_image = np.asanyarray(color_frame.get_data())
    hsl = cv2.cvtColor(color_image, cv2.COLOR_BGR2HLS)
    hsv = cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV)
    gray = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    #gray = 255-gray
    white_low = np.array([0, 150, 0], dtype=np.uint8)
    white_high = np.array([180, 255, 255], dtype=np.uint8)
    white_mask = cv2.inRange(hsl, white_low, white_high)
    black_low = np.array([0, 225, 0], dtype=np.uint8)
    ret, thresh = cv2.threshold(gray, x, 255, cv2.THRESH_BINARY_INV)
    blackwhite_mask = cv2.bitwise_or(thresh, white_mask)
    combined = cv2.bitwise_and(gray, gray, mask=blackwhite_mask)

    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=2)
    #thresh = cv2.bitwise_not(thresh)
    try:
        bigline = black_object(thresh, 0)
        smallline = black_object(thresh, 1)
        whiteline = find_white(white_mask)
        cv2.circle(color_image, whiteline[0], 5, (0, 0, 255), -1)
        cv2.circle(color_image, bigline[0], 5, (0, 0, 255), -1)
        cv2.circle(color_image, smallline[0], 5, (0, 0, 255), -1)
        #print(smallline)

    except:
        pass
    try:
        to_merge = (smallline[5], whiteline[3])
        pointlist = []
        for ctr in to_merge:
            pointlist += [pt[0] for pt in ctr]
        ctr = np.array(pointlist).reshape((-1,1,2)).astype(np.int32)
        ctr = cv2.convexHull(ctr)
        print(ctr)
        """cv2.drawContours(color_image, ctr, -1, (255, 0, 0), 6)
        cv2.drawContours(color_image, whiteline[3], -1, (0, 0, 255), 6)
        cv2.drawContours(color_image, smallline[5], -1, (0, 230, 255), 6)"""
        rows, cols = color_image.shape[:2]
        [vx, vy, x2, y] = cv2.fitLine(ctr, cv2.DIST_L2, 0, 0.01, 0.01)
        lefty = int((-x2 * vy / vx) + y)
        righty = int(((cols - x2) * vy / vx) + y)
        cv2.line(color_image, (cols - 1, righty), (0, lefty), (0, 255, 0), 2)







        cv2.circle(color_image, ball[0], 5, (0, 0, 255), -1)
    except:
        pass
    """try:
        yb = smallline[3][1]
        ya = smallline[4][1]
        xb = smallline[3][0]
        xa = smallline[4][0]
        tous = (yb - ya) / (xb - xa)
        print(tous)
        e = tous*(ball[1][0] - xa)
        print(tous, e)
        print(ball[1][1])

        try:
            if ball[1][1] > (ya - e):
                print("true")
            else:
                print("false")
        except:
            pass
    except:
        pass"""



    cv2.imshow("blk", thresh)
    cv2.imshow("n", color_image)
    cv2.imshow("wht", white_mask)
    cv2.imshow("combined", combined)

    key = cv2.waitKey(10)

    if key & 0xFF == ord("q"):
        cv2.destroyAllWindows()
        pipeline.stop()
        break
