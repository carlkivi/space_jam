import ImageProcessing as imp

print("welcome to the calibration tool")

while True:
    command = input("please enter command:")
    if command == "colors":
        imp.save_colors(4)
    elif command == "q":
        break
    elif command == "test":
        imp.robo_vision()
    else:
        command = input("invalid command, please try again")