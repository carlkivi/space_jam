import socket  # for sockets
import sys  # for exit
import keyboard
import time
"""
RC remote for the robot
RUN THIS CODE IN THE REMOTE PC AS SUDO
"""

# initialise constants
host = '192.168.1.171'
port = 63789
message = ""
buffer_size = 4096
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
reply = s.recv(buffer_size).decode()
print(reply)

while True:
    message = keyboard.read_key()
    if message != "":
        try:
            s.sendall(message.encode())
        except socket.error:
            print('...send failed...')
        reply = s.recv(buffer_size).decode()
    time.sleep(1/50)

s.close()

