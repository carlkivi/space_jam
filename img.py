import os
import sys
from threading import Thread
import cv2
import pyrealsense2 as rs
import numpy as np
import json
from functools import partial

import serial
from setuptools import glob


class thread:
    def __init__(self):
        # initialize variables
        self.running = True
        self.current_frame = None
        self.current_hsv_frame = None
        self.screen_width = 960
        self.screen_height = 540
        self.depth_width = 1280
        self.depth_height = 720

        # initialize pipeline
        self.pipeline = rs.pipeline()
        self.config = rs.config()
        self.config.enable_stream(rs.stream.color, self.screen_width, self.screen_height, rs.format.bgr8, 60)
        self.config.enable_stream(rs.stream.depth, self.depth_width, self.depth_height, rs.format.z16, 30)
        self.profile = self.pipeline.start(self.config)
        self.sensor = self.profile.get_device().query_sensors()[1]
        self.sensor.set_option(rs.option.enable_auto_exposure, False)
        self.sensor.set_option(rs.option.enable_auto_white_balance, False)
        self.align_to = rs.stream.color
        self.align = rs.align(self.align_to)



        # frames
        self.frames = self.pipeline.wait_for_frames()
        self.aligned_frames = self.align.process(self.frames)
        self.color_frame = self.aligned_frames.get_color_frame()
        self.current_frame = np.asanyarray(self.color_frame.get_data())
        self.depth_frame = self.aligned_frames.get_depth_frame()
        self.current_depth_frame = np.asanyarray(self.depth_frame.get_data())
        Thread(name = "img_thread", target = self.img_thread).start()


    def img_thread(self):
        while self.running:
            self.frames = self.pipeline.wait_for_frames()
            self.aligned_frames = self.align.process(self.frames)
            self.color_frame = self.aligned_frames.get_color_frame()
            self.current_frame = np.asanyarray(self.color_frame.get_data())
            self.current_hsv_frame = cv2.cvtColor(self.current_frame, cv2.COLOR_BGR2HSV)
            self.depth_frame = self.aligned_frames.get_depth_frame()

    def show_image(self):
        cv2.namedWindow("rs2", cv2.WINDOW_AUTOSIZE)
        frame = self.get_frame()
        cv2.imshow("rs2", frame)
        cv2.waitKey(1)

    def show_target_mask(self, target):
        cv2.namedWindow(target, cv2.WINDOW_AUTOSIZE)
        target_color = self.read_thresholds()[target]
        mask = cv2.inRange(self.get_hsv_frame(), tuple(target_color["min"]), tuple(target_color["max"]))
        cv2.imshow(target, mask)
        cv2.waitKey(1)

    def get_frame(self):
        return self.current_frame

    def get_hsv_frame(self):
        return self.current_hsv_frame

    def get_depth_frame(self):
        return self.depth_frame

    def get_object_distance(self, color):
        coords = self.get_coords(color)
        return self.depth_frame.get_distance(coords[0], coords[1])

    def get_kaugus(self, x, y):
        return self.depth_frame.get_distance(x, y)

    def set_stop(self):
        self.pipeline.stop()
        self.running = False

    def get_screen_center_x(self):
        return self.screen_width / 2

    def get_screem_center_y(self):
        return self.screen_height / 2

    def get_screen_res(self):
        return (self.screen_width, self.screen_height)

    def read_thresholds(self):
        path = os.path.dirname(os.path.realpath(__file__))
        file = path + "/colors.json"
        try:
            with open(file, "r") as f:
                saved_colors = json.loads(f.read())
        except FileNotFoundError:
            saved_colors = {}
        return saved_colors



    def find_object(self, target):
        # get color and create mask
        target_color = self.read_thresholds()[target]
        mask = cv2.inRange(self.get_hsv_frame(), tuple(target_color["min"]), tuple(target_color["max"]))
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        # find all the contours on the mask
        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) > 0:
            #find biggest contour and find its centerpoint
            c = max(contours, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))
            return [center, (x, y), radius]
        else:
            return None

    def black_object(self, n):
        #mustade objektide otsimiseks töötleb pildi mustvalgeks
        gray = cv2.cvtColor(self.get_current_frame(), cv2.COLOR_BGR2GRAY)
        #musta tuvastus on hardcoded, muuda funktsiooni teist parameetrit korrigeerimiseks
        ret, thresh = cv2.threshold(gray, 80, 255, cv2.THRESH_BINARY_INV)
        # find all the contours on the thresh
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) > 0:
            c = sorted(contours, key=cv2.contourArea, reverse=True)

            maxc = max(c[n].tolist())[0]
            minc = min(c[n].tolist())[0]

            ((x, y), radius) = cv2.minEnclosingCircle(c[n])
            M = cv2.moments(c[n])
            center = (round(M["m10"] / M["m00"]), round(M["m01"] / M["m00"]))
            return [center, (x, y), radius, maxc, minc]
        else:
            return None


    def platsil(self, object):
        return True
        try:
            smallline = self.black_object(1)
        except:
            return None

        try:
            yb = smallline[3][1]
            ya = smallline[4][1]
            xb = smallline[3][0]
            xa = smallline[4][0]
            tous = (yb - ya) / (xb - xa)
            e = tous * (object[0] - xa)
            print(tous, e)
            print(object[1])

            try:
                if object[1] > (ya - e):
                    print("true")
                    return True
                else:
                    print("false")
                    return False
            except:
                pass
        except:
            pass



    def get_coords(self, target):
        balls = self.find_object(target)
        center = balls[0]
        if balls[2] > 5:
            return center
        else:
            return None