import time
from collections import deque
import img
import mainboard
import server

board = mainboard.thread_board()
ip = img.thread()

ID = "Io"
ref_commands = {}

state = "auto"  #kui init_server False siis ära seda manualiks muuda
logic_state = "searching"
init_server = False     #Kui tahad koodi testida ilma serverita, else True
x_speed = 10
y_speed = 10
center_offset = 15 #mitu pikslit me lubame kaameral keskelt nihkes olla
basket_offset = 50
#screen_center = ip.get_screen_center_x()
#kuna kaamera nihkes siis
screen_center = 532
ball = None

#vajalikud värvid colors.json failist
ball_color = "green"
self_basket = "blue"
target_basket = "pink"
border_color = ""

just_started_moving = True #aja mõõtmiseks booleanid
just_started_searching = True
lõpphetk = 0
max_search_time = 10 # sekundid
max_dist_while_searching = 1 # kui lähedale korvile me otsides lähme(meetrites)
home_enemy = False #valib kumma korvi juurde robot otsima läheb
min_dist_from_ball = 0.45 #kaugus mil me võiks palli hakata korjama (meetrites)
max_viskekaugus = 1.5 #max kaugus mil robot üritab palli visata(meetrites)




def execRCcmd(cmd):
    global state
    data = cmd
    if data == "a":
        state = "auto"
    if data == "up":
        board.omni_move(0, -y_speed, 0)
    if data == "down":
        board.omni_move(0, y_speed, 0)
    if data == "left":
        mainboard.rotate(-x_speed)
    if data == "right":
        mainboard.rotate(x_speed)
    if data == "q":
        state = "exit"

def referee_cmds(commads):
    global self_basket, target_basket, state
    try:
        if commads["signal"] == "stop":
            state = "exit"
        elif commads["signal"] == "start":
            state = "auto"

        index = 0

        if commads["targets"]:
            for i in commads["targets"]:
                if i == ID:
                    break
                index+=1

        if index == 0:
            self_basket = commads["baskets"][0]
            target_basket = commads["baskets"][1]

        else:
            self_basket = commads["baskets"][1]
            target_basket = commads["baskets"][0]


    except:
        pass
        #print("error in REF CMDS")






def move_towards(coords):
    global center_offset
    if coords[0] > screen_center + center_offset:
        # right
        board.omni_move(x_speed, -y_speed, 0)
    elif coords[0] < screen_center - center_offset:
        # left
        board.omni_move(-x_speed, -y_speed, 0)
    else:
        # forward
        board.omni_move(0, -y_speed, 0)


command_queue = deque()
if init_server:
    try:
        server.start_socket_server(command_queue, ref_commands)
    except:
        print("Exiting because of error on initialize")
        exit(0)

while True:
    #check for changes in referee commands
    referee_cmds(ref_commands)

    if state == "manual":
        print(
            """
            Remote commands:

            arrows for driving the robot

            a- change state to auto
            q- to safely shut down robot
            """)
        while True:
            data = ""
            data = command_queue.pop()
            if data == "":
                board.stop()
                continue
            else:
                execRCcmd(data)
            if state == "auto" or state == "exit":
                break


    if state == "auto":
        print(
            """
            state is now autonomous
            Remote commands:

            q- to shut down robot
            m- go back to manual state
            """
        )
        while True:
            referee_cmds(ref_commands)
            data = ""
            try:
                data = command_queue.pop()
            except:
                pass
            if data == "q":
                state = "exit"
                break
            elif data == "m":
                state = "manual"
                break

            #This is where the game logic begins

            #Esimese asjana on vaja leida sobiv pall
            if logic_state == "searching":
                ball = None
                print("searching")
                if just_started_searching:
                    lõpphetk = time.process_time() + max_search_time
                    just_started_searching = False
                try:
                    ball = ip.get_coords(ball_color)
                except:
                    board.rotate(x_speed)
                praegune_hetk = time.process_time()
                if praegune_hetk >= lõpphetk:
                    print("otsimise aeg täis")
                    if not home_enemy:
                        logic_state = "move home"
                        home_enemy = True
                        continue
                    else:
                        logic_state = "move enemy"
                        home_enemy = False
                        continue
                if ball == None:
                    continue
                else:
                    if ip.platsil(ball) == True:
                        logic_state = "getting ball"
                        just_started_moving = True
                        board.stop()
                    else:
                        continue
                    # kui true siis logic state = getting ball
                    #kindlasti board.stop ka, et me mööda ei liiguks
                    #kui oleme liiga kaua otsinud hakkame korvi poole liikuma aka vahetame logic state



            #Ei leidnud algul palli liigume enda korvi juurde ja otsime uuesti
            elif logic_state == "move home":
                print("liigun enda korvi poole")
                friendly_basket = None
                try:
                    friendly_basket = ip.get_coords(self_basket)
                except:
                    board.rotate(-x_speed)
                if friendly_basket == None:
                    continue
                else:
                    distance_from_basket = ip.get_kaugus(friendly_basket[0], friendly_basket[1])
                    print(distance_from_basket)
                    if distance_from_basket != 0 and distance_from_basket > max_dist_while_searching:
                        move_towards(friendly_basket)
                    elif distance_from_basket != 0:
                        logic_state = "searching"
                        just_started_searching = True
                        continue


            #Ei leidnud palli liigume vastase korvi juurde ja otsime uuesti
            elif logic_state == "move enemy":
                print("liigun vastase korvi poole")
                enemy_basket = None
                try:
                    enemy_basket = ip.get_coords(target_basket)
                except:
                    board.rotate(x_speed)
                if enemy_basket == None:
                    continue
                else:
                    distance_from_basket = ip.get_kaugus(enemy_basket[0], enemy_basket[1])
                    print(distance_from_basket)
                    if distance_from_basket != 0 and distance_from_basket > max_dist_while_searching:
                        move_towards(enemy_basket)
                    elif distance_from_basket != 0:
                        logic_state = "searching"
                        just_started_searching = True
                        continue


            #Lõpuks oleme leidnud sobiva palli, liigume selle juurde
            elif logic_state == "getting ball":
                print("proovin palli poole liikuda")
                if just_started_moving:
                    lõpphetk = time.process_time() + max_search_time
                    just_started_moving = False
                try:
                    ball = ip.get_coords(ball_color)
                except:
                    pass
                paregune_hetk = time.process_time()
                if praegune_hetk > lõpphetk:
                    logic_state = "searching"
                    just_started_searching = True
                    break
                if ball == None:
                    continue
                else:
                    dist_from_ball = ip.get_kaugus(ball[0],ball[1])
                    print(dist_from_ball)
                    #liigume pallini
                    if dist_from_ball != 0 and dist_from_ball > min_dist_from_ball:
                        move_towards(ball)
                    #püüame kinni palli
                    elif dist_from_ball != 0:
                        board.stop()
                        print("katsun palli haarata")
                        if not board.get_sensor_status():
                            move_towards(ball)
                            board.grab()
                        else:
                            board.stop()
                            logic_state = "throwing"
                        #siia peab lisama palli kinni püüdmise koodi
                        #kui pall käes, siis logic state = throwing
                        pass


            #Hakkame palli viskama, keerame korvi poole ja viskame
            #Kui oleme teises platsi otsas siis liigume lähemale palliga
            elif logic_state == "throwing":
                print("asun viskele")
                try:
                    enemy_basket = ip.get_coords(target_basket)
                except:
                    board.rotate(-x_speed)
                    continue
                if enemy_basket == None:
                    continue
                else:
                    distance_from_basket = ip.get_kaugus(enemy_basket[0], enemy_basket[1])
                    print(distance_from_basket)
                    if enemy_basket[0] > screen_center + basket_offset:
                        board.rotate(-x_speed) #vasakule
                    elif enemy_basket[0] < screen_center - basket_offset:
                        board.rotate(x_speed) # paremale
                    elif distance_from_basket != 0 and distance_from_basket > max_viskekaugus:
                        #liigume lähemale
                        move_towards(enemy_basket)
                    elif distance_from_basket != 0:
                        print("proovin visata kauguselt: ", distance_from_basket)
                        if not board.get_sensor_status():
                            board.stop()
                            logic_state = "searching"
                            just_started_searching = True
                        else:
                            board.throw(5000)
                            time.sleep(1)
                            board.grab()
                        #läheb vaja valemit throweri käivitamiseks kauguse põhjal
                        #kui pall visatud siis logic state = searching
                        pass


    if state == "exit":
            break

server.shut_down_server()
board.stop()
print("Safely shut down robot")
exit(0)