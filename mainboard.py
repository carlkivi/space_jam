import serial
import sys
import glob
import time
from threading import Thread
import numpy as np
import math
import struct

def LimitFrames(maxPerSecond):
    minInterval = 1.0 / float(maxPerSecond)

    def decorate(func):
        lastTimeCalled = [0.0]

        def rateLimitedFunction(*args, **kwargs):
            elapsed = time.process_time() - lastTimeCalled[0]
            leftToWait = minInterval - elapsed
            if leftToWait > 0:
                time.sleep(leftToWait)
            ret = func(*args, **kwargs)
            lastTimeCalled[0] = time.process_time()
            return ret
        return rateLimitedFunction
    return decorate


class thread_board:
    def __init__(self):
        # init connection
        self.board = serial.Serial(self.serial_ports()[0], 115200, timeout=.1)
        self.board.flushInput()
        self.board.flushOutput()
        self.running = True
        self.sensor = False
        self.speeds = [0]*5
        print(self.serial_ports())
        Thread(name="mainboard", target=self.main).start()



    def main(self):
        while self.running:
            resp = self.board.readall()
            try:
                resp = str(resp).split("\\xaa\\xaa")
                cmd = resp[-2]
                cmd = cmd[-5:-4]
                print(cmd)
                if cmd == "1":
                    self.sensor = True
                else:
                    self.sensor = False
            except:
                pass
            self.board.flushOutput()
            self.board.flushInput()

    def get_sensor_status(self):
        return self.sensor

    def serial_ports(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    @LimitFrames(40)  # 40 per sec
    def omni_move(self, speed_x, speed_y, vel_angular):
        speeds = self.omni_speeds(speed_x, speed_y, vel_angular)
        self.speeds[0] = round(speeds[1])
        self.speeds[1] = round(speeds[2])
        self.speeds[2] = round(speeds[0])
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1],self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA ))


    def omni_speeds(self, speed_x, speed_y, vel_angular):
        angle_rad = 90
        robot_speed = np.sqrt(speed_x * speed_x + speed_y * speed_y)
        dir_angle = math.atan2(speed_y, speed_x)
        angles_of_wheels = np.radians(np.array([240, 120, 0]))
        # angles_rad = np.radians(np.array([angle_rad, angle_rad, angle_rad]))
        speeds = np.array([robot_speed, robot_speed, robot_speed])
        distances = np.array([0.15, 0.155, 0.15])
        angular_vels = np.array([vel_angular, vel_angular, vel_angular])
        speeds_of_wheels = np.round(speeds * np.cos(dir_angle - angles_of_wheels) + distances * -angular_vels, 2)
        return speeds_of_wheels

    @LimitFrames(40)  # 40 per sec
    def throw(self, velocity):
        if 3000 <= velocity <= 6400:
            self.speeds[3] = velocity
            self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA))
        else:
            print("thrower speed has to be between 3000-6400")


    @LimitFrames(40)
    def grab(self):
        self.speeds[4] = 1000  #servo speed
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA))


    @LimitFrames(40)  # 40 per sec
    def rotate(self, speed):
        self.speeds[0] = speed
        self.speeds[1] = speed
        self.speeds[2] = speed
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA))


    @LimitFrames(40)  # 40 per sec
    def stop(self):
        self.speeds[0] = 0
        self.speeds[1] = 0
        self.speeds[2] = 0
        self.speeds[3] = 3000
        self.speeds[4] = 0
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA))

    @LimitFrames(40)
    def stop_wheels(self):
        self.speeds[0] = 0
        self.speeds[1] = 0
        self.speeds[2] = 0
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4], 0xAA, 0xAA))

    @LimitFrames(40)
    def stop_thrower(self):
        self.speeds[3] = 3000
        self.board.write(struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4] , 0xAA, 0xAA))

    @LimitFrames(40)
    def stop_servo(self):
        self.speeds[4] = 0
        self.board.write(
            struct.pack("<hhhhhBB", self.speeds[0], self.speeds[1], self.speeds[2], self.speeds[3], self.speeds[4],
                        0xAA, 0xAA))

"""
Omni move
-x liigutab vasakule
x liigutab paremale
y tagurdab
-y otse

"""