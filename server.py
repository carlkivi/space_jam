import socket
import sys
from _thread import *
from collections import deque
import time
import websocket
import json

WebSocket_URL = "ws://echo.websocket.org/"
"""
HOST = socket.gethostbyname(socket.gethostname()+".local")
PORT = 63789  # arbitrary port
buffer_size = 4096  # buffer size
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('...socket created...')
print("...waiting for remote...")
print("Accepting connections on:", HOST)
s.bind((HOST, PORT))
s.listen(3)
"""
ws = websocket.create_connection(WebSocket_URL)


def shut_down_server():
    s.close()
    ws.close()


def clientthread(conn, output_queue):
    conn.send('I am robot. Enter command\n'.encode())
    while True:
        try:
            data = conn.recv(buffer_size)
            if not data:
                break
            output_queue.append(data.decode())
            reply = '...OK... ' + data.decode()
            conn.sendall(reply.encode())
        except socket.error as message:
            print(message)
            break
        time.sleep(1/50)
    conn.close()


def get_commands(ws, referee_cmds):
    ws.send(json.dumps({
        "signal": "start",
        "targets": ["Io", "001TRT"],
        "baskets": ["magenta", "blue"]
    }))
    while True:
        referee_cmds = ws.recv()
        print(referee_cmds)
        time.sleep(1/50)




def start_socket_server(queue, commands):
    #conn, addr = s.accept()
    #print('...connected with ' + addr[0] + ':' + str(addr[1]))
    #start_new_thread(clientthread, (conn, queue))
    start_new_thread(get_commands, (ws, commands))
