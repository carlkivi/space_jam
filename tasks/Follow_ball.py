import time
import mainboard
import img

board = mainboard.thread_board()
ip = img.thread()
speed = 5
x_speed = 5
y_speed = 5
dist_from_target = 100
screen_center = ip.get_screen_center_x()
center_offset = 30
ball_color = "home_green"
done = False

# OMNI: kujuta ette koordinaatteljestikul liikumist
# mööda x telge saad paremale ja vasakule liikuda
# y telge pidi saab edasi ja tagasi liikuda
# kombineerides saab diagonaale võtta



while True:
    try:
        ip.show_target_mask(ball_color)
        coords = ip.get_coords(ball_color)
    except:
        board.rotate(speed)
        continue
    if coords == None:
        continue
    else:
        dist_from_target = ip.get_kaugus(coords[0], coords[1])
        print(dist_from_target)
        if dist_from_target < 0.4:
            if done:
                print("kohal")
                continue
            else:
                done = True
                board.omni_move(0,0,0)
        else:
            done = False
        if coords[0] > screen_center + center_offset:
            #right
            board.omni_move(-x_speed, -y_speed, 0)
        elif coords[0] < screen_center - center_offset:
            #left
            board.omni_move(x_speed, -y_speed, 0)
        else:
            #forward
            board.omni_move(0, -y_speed, 0)
    time.sleep(.1)