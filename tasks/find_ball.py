import time

import mainboard
import img

#See task on valmis, vaja ette näidata vb vaja platsil veidi confida enne ette näitamist
#kui programm viskab errori ss unplugi USBid ja ühenda uuesti samasse pessa

board = mainboard.thread_board()
ip = img.thread()
speed = 5
screen_center = ip.get_screen_center_x()
center_offset = 30
ball_color = "home_green"
done = False
# rotate paremale = +speed


while True:
    # proovib saada palli koordinaate
    try:
        coords = ip.get_coords(ball_color)
    except:
        continue
    #näitab pilti, mida robot näeb
    ip.show_target_mask(ball_color)
    if coords == None:
        #palli pole
        print("searching")
        board.rotate(speed)
    else:
        #keerame vasakule
        if coords[0] < screen_center-center_offset:
            board.rotate(-speed)
        #keerame paremale
        elif coords[0] > screen_center+center_offset:
            board.rotate(speed)
        else:
        #jõudsime pallini, väldime konstanset mainboardile kirjutamist
            if done:
                print("done")
                continue
            else:
                board.stop()
                done = True
    done = False
    time.sleep(.1)