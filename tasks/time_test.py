import time


def LimitFrames(maxPerSecond):
    minInterval = 1.0 / float(maxPerSecond)
    def decorate(func):
        lastTimeCalled = [0.0]
        def rateLimitedFunction(*args, **kargs):
            elapsed = time.process_time() - lastTimeCalled[0]
            leftToWait = minInterval - elapsed
            if leftToWait > 0:
                time.sleep(leftToWait)
            ret = func(*args, **kargs)
            lastTimeCalled[0] = time.process_time()
            return ret
        return rateLimitedFunction
    return decorate


@LimitFrames(1)
def test(num):
    print(num)


i = 0
kogu_aeg = 0
for j in range(10):
    algus = time.process_time()
    test(i)
    i+=1
    lõpp = time.process_time()
    kogu_aeg += lõpp-algus
    print("proc on jooksnud:", kogu_aeg)